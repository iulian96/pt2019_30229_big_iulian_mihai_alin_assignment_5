import java.time.Duration;
import java.time.LocalDateTime;

public class MonitoredData {

	private String content;

	private List<String> contents;

	public ConstructorReference() {
		this.content = "created by constructor reference";
	}

	public ConstructorReference(String content) {
		this.content = content;
	}

	public ConstructorReference(List<String> contents) {
		this.contents = contents;
	}

	public String getContent()
	{
		return content;
	}

	public List<String> getContents()
	{
		return contents;
	}

	public static void main(String... args)
	{
		ConstructorReferenceSAM<ConstructorReference> constructorSam = ConstructorReference::new;
		System.out.println("\n\n\tcontent = "+constructorSam.whatEverMethodName().getContent());
		
		ConstructorReferenceSAMWithArgs<ConstructorReference,String> constructorSamWithArg = ConstructorReference::new;		
		System.out.println("\n\n\tcontent by arg = "+constructorSamWithArg.createMeANewObject("created by constructor reference with arg").getContent());
		
		ConstructorReferenceSAMWithParameterizedArg<ConstructorReference,String> constructorSamWithParameterizedArg = ConstructorReference::<String>new;
		System.out.println("\n\n\tcontents size by parameterized arg = "+constructorSamWithParameterizedArg.magic(new ArrayList<String>()).getContents().size());
		System.out.println("");
		
	}
}
	public double getTimp() {
		double period = 0;

		VariableCaptureInClosureSAM anonymousClass = new VariableCaptureInClosureSAM()
		{
			MutableObject mutable = new MutableObject();

			@Override
			public MutableObject retrieveMutable()
			{
				this.mutable.setContent("from_anonymous");
				return this.mutable;
			}

		};

		return time;

}

	}

	public int sub() {
		String[] splite = getEndTime().split("\t");
		ScopingOfThis scopingOfThis = new ScopingOfThis();
		VariableCaptureInClosureSAM sam = scopingOfThis.createClosure();

		MutableObject mutableFromClosure = sam.retrieveMutable();
		System.out.println("\nmutableFromClosure content = " + mutableFromClosure.getContent());

		MutableObject mutableFromAnonymous = scopingOfThis.createAnonymous().retrieveMutable();
		System.out.println("mutableFromAnonymous content = " + mutableFromAnonymous.getContent());
System.out.println("");
		}
		return 0;

	}

	public String toString() {
		String[] splita = getActivity().split("\t");

		setActivity(splita[0]);
		return "Activitatea " + getActivity() + " a inceput la " + getStartTime() + " si s-a sfarsit la "
				+ getEndTime();
	}

}