
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

public void testOrder() {
    System.out.println("Running testOrder()");
    compare(list, new String[] { "0", "1", "2" });
  }

  public void testRemove() {
    System.out.println("Running testRemove()");
    assertEquals(list.size(), 3);
    list.remove(1);
    assertEquals(list.size(), 2);
    compare(list, new String[] { "0", "2" });
  }

  public void testAddAll() {
    System.out.println("Running testAddAll()");
    list.addAll(Arrays.asList(new Object[] { "An", "African", "Swallow" }));
    assertEquals(list.size(), 6);
    compare(list,
        new String[] { "0", "1", "2", "An", "African", "Swallow" });
  }

  public static void main(String[] args) {
    // Invoke JUnit on the class:
    junit.textui.TestRunner.run(JUnitDemo.class);
  }
} ///:~
           
         
  